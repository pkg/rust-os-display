Source: rust-os-display
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-unicode-width-0.1+default-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ryan Gonzalez <ryan.gonzalez@collabora.com>
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/os-display]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/os-display
X-Cargo-Crate: os_display
Rules-Requires-Root: no

Package: librust-os-display-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-unicode-width-0.1+default-dev
Recommends:
 librust-os-display+default-dev (= ${binary:Version})
Provides:
 librust-os-display+alloc-dev (= ${binary:Version}),
 librust-os-display+native-dev (= ${binary:Version}),
 librust-os-display+std-dev (= ${binary:Version}),
 librust-os-display+unix-dev (= ${binary:Version}),
 librust-os-display+windows-dev (= ${binary:Version}),
 librust-os-display-0-dev (= ${binary:Version}),
 librust-os-display-0+alloc-dev (= ${binary:Version}),
 librust-os-display-0+native-dev (= ${binary:Version}),
 librust-os-display-0+std-dev (= ${binary:Version}),
 librust-os-display-0+unix-dev (= ${binary:Version}),
 librust-os-display-0+windows-dev (= ${binary:Version}),
 librust-os-display-0.1-dev (= ${binary:Version}),
 librust-os-display-0.1+alloc-dev (= ${binary:Version}),
 librust-os-display-0.1+native-dev (= ${binary:Version}),
 librust-os-display-0.1+std-dev (= ${binary:Version}),
 librust-os-display-0.1+unix-dev (= ${binary:Version}),
 librust-os-display-0.1+windows-dev (= ${binary:Version}),
 librust-os-display-0.1.3-dev (= ${binary:Version}),
 librust-os-display-0.1.3+alloc-dev (= ${binary:Version}),
 librust-os-display-0.1.3+native-dev (= ${binary:Version}),
 librust-os-display-0.1.3+std-dev (= ${binary:Version}),
 librust-os-display-0.1.3+unix-dev (= ${binary:Version}),
 librust-os-display-0.1.3+windows-dev (= ${binary:Version})
Description: Display strings in a safe platform-appropriate way - Rust source code
 This package contains the source for the Rust os_display crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-os-display+default-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-os-display-dev (= ${binary:Version}),
 librust-os-display+native-dev (= ${binary:Version}),
 librust-os-display+alloc-dev (= ${binary:Version}),
 librust-os-display+std-dev (= ${binary:Version})
Provides:
 librust-os-display-0+default-dev (= ${binary:Version}),
 librust-os-display-0.1+default-dev (= ${binary:Version}),
 librust-os-display-0.1.3+default-dev (= ${binary:Version})
Description: Display strings in a safe platform-appropriate way - feature "default"
 This metapackage enables feature "default" for the Rust os_display crate, by
 pulling in any additional dependencies needed by that feature.
